import math as mt
class linearCualitativeData():
    def __init__(self,a):
        self.data = a
    def settable(self):
        self.total =0
        self.table = dict()
        keys = set(self.data)
        for i in keys:
            h = self.data.count(i)
            self.table[i] = h
            self.total = self.total + h
    def gettable(self):
        try:
            return self.table
        except AttributeError:
            return None
    def gettotal(self):
        try:
            return self.total
        except AttributeError:
            return 0
class linearQuantitativeData():
    def __init__(self,d):
        self.data = d
        self.total = len(self.data)
    def settable(self):
        self.table = dict()
        keys = set(self.data)
        for i in keys:
            h = self.data.count(i)
            self.table[i] = h
    def gettable(self):
        try:
            return self.table
        except AttributeError:
            return None
    def gettotal(self):
        try:
            return self.total
        except AttributeError:
            return 0
    def setmean(self):
        self.mean =0
        self.sumtotal = 0
        for i in self.data:
            self.sumtotal = self.sumtotal + i
        self.mean = self.sumtotal/self.total
    def getmean(self):
        try:
            return self.mean
        except AttributeError:
            return 0
    def setmedian(self):
        self.median = 0
        datatem = sorted(self.data)
        # print(datatem)
        if(self.total %2 ==0):
            self.median = float(float((datatem[int(self.total/2)] + datatem[(int(self.total/2))-1]))/2)
        else:
            self.median = datatem[int((self.total)/2)]
    def getmedian(self):
        try:
            return self.median
        except AttributeError:
            return 0
    def setmode(self):
        self.settable()
        m = self.table[self.table.keys()[0]]    
        for i in self.table:
            if self.table[i]> m:
                m = i
        self.mode =m
    def getmode(self):
        try:
            return self.mode
        except AttributeError:
            return 0
    def setintervals(self):
        keys =[]
        datas = []
        brand =[]
        datasacum =[]
        por =[]
        poracum =[] 
        F =0
        H =0
        k = int(1 + (3.3*(mt.log(self.total,10)))+1)
        r = max(self.data) - min(self.data)
        a = int(r/k)+1
        j = a*k
        d = (j-r)/2
        start = min(self.data) - d
        fin = start + a
        for i in range(k):
            keys.append("[" + str(start) +", " + str(fin) + ")")
            data = 0;
            for i in self.data: 
                if i < fin and i>= start:
                    data = data +1
            F = F + data
            H = float(float(F)/self.total)
            datasacum.append(F)
            por.append(float(float(data)/self.total))
            poracum.append(H)
            brand.append(int((fin + start)/2))
            datas.append(data)
            start = start+a
            fin = fin +a
        self.intervals = zip(keys,brand,datas,datasacum,por,poracum)
    def getintervals(self):
        try:
            return self.intervals
        except AttributeError:
            return 0
    def printintervals(self):
        print("interval  Ci  fi  Fi  hi   Hi")
        for i in self.intervals:
            print(i[0] + "  " + str(i[1]) + "  " + str(i[2])+ "   " + str(i[3])+ "  " + str(round(i[4],2))+ "  " + str(round(i[5],2)))
    def setrange(self):
        self.range = max(self.data) - min(self.data)
    def getrange(self):
        try:
            return self.range
        except AttributeError:
            return 0
    def setvariance(self):
        if self.getmean() == 0:
            self.setmean()
        self.variance =0
        su = 0
        for i in self.data:
            su = su + ((i-self.mean)**2)
        self.variance = float(float(su)/(self.total-1))
    def getvariance(self):
        try:
            return self.variance
        except AttributeError:
            return 0
    def setstandarddeviation(self):
        if self.getvariance() ==0:
            self.setvariance()
        self.sdeviation = float(mt.sqrt(self.variance))
    def getstandarddeviation(self):
        try:
            return self.sdeviation
        except AttributeError:
            return 0
    def setvariationcoefficient(self):
        if self.getstandarddeviation() ==0:
            self.setstandarddeviation()
        if self.getmean() == 0:
            self.setmean()
        self.vcoefficient = float(float(self.sdeviation)/float(self.mean))*100
    def getvariationcoefficient(self):
        try:
            return self.vcoefficient
        except AttributeError:
            return 0
               